/**
* @developer: Ruvin Roshan
* @library: Angularjs
*/
// JavaScript //
window.addEvent('domready', function () {
// Removed placeholder on focus and get back after blur 
jQuery('input,textarea').focus(function () {
jQuery(this).data('placeholder', jQuery(this).attr('placeholder'))
.attr('placeholder', '');
}).blur(function () {
jQuery(this).attr('placeholder', jQuery(this).data('placeholder'));
});
/**********************************************###
***************************************/

});

// AngularJS //
(function(angular) {
'use strict';



/* Directives - Repeat password 
*/
angular.module('myApp.checkpassword', [])
.directive('pwCheck', [function () {
return {
require: 'ngModel',
link: function (scope, elem, attrs, ctrl) {
var firstPassword = '#' + attrs.pwCheck;
//var firstPassword = document.getElementById(attrs.pwCheck).value;

elem.add(firstPassword).on('keyup', function () {
scope.$apply(function () {
ctrl.$setValidity('pwmatch', elem.val() === jQuery(firstPassword).val());
});
});
}
}
}]);



// Payment slip upload form
///////////////////##############- user registration -################//////////////////////////////////////
var app = angular.module("ibookingPayment", ['ngLoadingSpinner','ngMessages','myApp.checkpassword','udpCaptcha']);
app.controller("ibookingPaymentController", function($scope,$http,$window,$captcha) {





$scope.submit_form = function() {
//jQuery('.err_msg').hide();


//correct
if($captcha.checkResult($scope.resultado) == true)
{
////////////#################
$scope.method = 'POST';
$scope.url = 'index.php?option=com_ibooking&task=payment.checkState';
$scope.dataArray = {data:$scope.fields};

$http({method: $scope.method, url: $scope.url,data: $scope.dataArray}). 
then(function(response) {
$scope.status = response.status;

console.log(response.data);

if(response.data.state.check === true ){
$window.location.href = 'index.php?option=com_ibooking&view=payment&layout=payment_upload&param='+ response.data.state.info;
}else if(response.data.state.check === false ){
alert(response.data.state.msg);
$scope.fields.reference_number = '';
$scope.fields.nic = '';
}else{
// error
alert('Server Error occured !');
//$window.location.href = 'index.php';	
}






}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
alert('Request failed !, Please try again later.');
});
////////////#################
}
//error captcha
else
{
alert("Captcha Error: Please enter correct captcha !");
}


};
///////////////////##############-end-################//////////////////////////////////////






}); // app controller
// END - Payment slip upload form




















// Registration Module and Controller
///////////////////##############- user registration -################//////////////////////////////////////
var app = angular.module("ibookingRegistration", ['ngLoadingSpinner','ngMessages','myApp.checkpassword','udpCaptcha']);
app.controller("ibookingRegistrationController", function($scope,$http,$window,$captcha) {


get_existing_calendar_data();


////////////
$scope.hours = []; // list of intervals, eg: 12:00 am, 12:30 am, 1:00 am

$scope.start_time = new Date(), $scope.start_time.setHours(9), $scope.start_time.setMinutes(0), $scope.start_time.setSeconds(0);
$scope.end_time = new Date(), $scope.end_time.setHours(9), $scope.end_time.setMinutes(0), $scope.end_time.setSeconds(0);

for (var i = 0; i < 24; i++) {
// push interval of times at every half hour
var temp_date = new Date();
temp_date.setHours(i);
temp_date.setMinutes(0);
temp_date.setSeconds(0);
$scope.hours.push(temp_date);

var temp_date2 = new Date();
temp_date2.setHours(i);
temp_date2.setMinutes(30);
temp_date2.setSeconds(0);
$scope.hours.push(temp_date2);
}


$scope.update = function (time, index, slot) {

/*var start_time = jQuery('#start_time').val();
var end_time = jQuery('#end_time').val();
console.log(start_time);
console.log(end_time);*/
// To be finished: update workingHours array with new times selected
//console.log(time, index, slot);
}




function get_existing_calendar_data(){

var getParam = jQuery('#param').val(); //$scope.fields.param; // get param from hidden field


var formObj = {};
formObj['param'] = getParam;
formObj['form'] = $scope.fields;


$scope.method = 'POST';
$scope.url = 'index.php?option=com_ibooking&task=show_calendar.getExistingCalendarData';
$scope.dataArray = {data:$scope.fields};

$http({method: $scope.method, url: $scope.url,data: formObj}). 
then(function(response) {
$scope.status = response.status;


jQuery('#calendar').availabilityCalendar(response.data);


if(response.data.state==false){
// check existing data
//alert(response.data.msg);
//jQuery('.err_msg').show().html(response.data.msg);

}else if(response.data.state==true){
// success
//alert('Successfully registered.');

}else{
// error
//alert('Registration Error occured !');
//$window.location.href = 'index.php';
}

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});



}



















$scope.place_booking = function() {

var getParam = jQuery('#param').val(); //$scope.fields.param; // get param from hidden field


var formObj = {};
formObj['param'] = getParam;
formObj['form'] = $scope.fields;

var chekBookValid = validate_booking();


if(chekBookValid === true){
jQuery('.err_msg').show().html('Please Wait..!')
$scope.method = 'POST';
$scope.url = 'index.php?option=com_ibooking&task=show_calendar.getState';
$scope.dataArray = {data:$scope.fields};

$http({method: $scope.method, url: $scope.url,data: formObj}). 
then(function(response) {
$scope.status = response.status;



if(response.data.state === false){
// check existing data
alert(response.data.msg);
jQuery('.err_msg').show().html(response.data.msg);

}else if(response.data.state==true){
jQuery('.err_msg').hide();
// success
alert('Successfully registered.');
//jQuery('.err_msg').show().html('Successfully registered.');
//$window.location.href = 'index.php?option=com_ibooking&view=show_calendar&layout=success&info='+response.data.info;
}else{
// error
alert('Registration Error occured !');
//$window.location.href = 'index.php';
}

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});


}





} // end method








function validate_booking() {
var return_set = false;
jQuery('#err-msg').hide();

var number_of_room = jQuery('#number_of_room').val();
var last_name_with_initials = jQuery('#last_name_with_initials').val();
var permanent_address = jQuery('#permanent_address').val();
var nic = jQuery('#nic').val();
var contact_no_mobile = jQuery('#contact_no_mobile').val();
var contact_no_home = jQuery('#contact_no_home').val();
var email = jQuery('#email').val();


if(
number_of_room == '' ||  
last_name_with_initials == '' || 
permanent_address == '' || 
nic == '' || 
contact_no_mobile == '' || 
email == '' //|| 
//contact_no_home == '' 

) {

jQuery('#err-msg').show().html('All fields required !');
alert('All fields required !');

}else{
return_set = true;	
jQuery('#err-msg').hide();
}



return return_set;
}










$scope.check_availability_book = function(fields) {



var propertytypeid = jQuery('#property_type_id').val();
var checkVal = validate(propertytypeid);

if(checkVal === true){

//$scope.fields.booking_start_date = $scope.year+'-'+$scope.month.id+'-'+$scope.day;
$scope.fields.booking_start_date = jQuery('#booking_start_date').val();
$scope.fields.booking_end_date = jQuery('#booking_end_date').val();
$scope.fields.property_type_id = propertytypeid;
/*$scope.fields.sector_id = jQuery('#sector_id').val();
$scope.fields.property_id = jQuery('#property_id').val();
*/
$scope.fields.start_time = jQuery('#start_time').val();
$scope.fields.end_time = jQuery('#end_time').val();


var jsonStr = JSON.stringify($scope.fields);
//console.log(jsonStr);
var encodedString = Base64(jsonStr,'encode'); //Base64.encode(jsonStr);

//console.log(encodedString);
//console.log($scope.fields);

$scope.method = 'POST';
$scope.url = 'index.php?option=com_ibooking&task=show_calendar.check_alreadyExistsDate_ajax';
$scope.dataArray = {data:$scope.fields};

$http({method: $scope.method, url: $scope.url,data: $scope.dataArray}). 
then(function(response) {
$scope.status = response.status;
if(response.data.state==false){
// check existing data
//jQuery('.err_msg').show().html(response.data.msg);
$window.location.href = 'index.php?option=com_ibooking&view=show_calendar&param='+encodedString;
}else if(response.data.state==true){
// success
alert(response.data.msg);
jQuery('#err-msg').show().html(response.data.msg);

}else{
// error
alert('Error occured !');
}

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});


} // validate



}



function ConvertTimeformat(format, str) {
var time = str;
var hours = Number(time.match(/^(\d+)/)[1]);
var minutes = Number(time.match(/:(\d+)/)[1]);
var AMPM = time.match(/\s(.*)$/)[1];
if (AMPM == "PM" && hours < 12) hours = hours + 12;
if (AMPM == "AM" && hours == 12) hours = hours - 12;
var sHours = hours.toString();
var sMinutes = minutes.toString();
if (hours < 10) sHours = "0" + sHours;
if (minutes < 10) sMinutes = "0" + sMinutes;
var resul = sHours + ":" + sMinutes;

var minutesVal = minutes/60;
var resl = hours+minutesVal;

return resl; // return hrs
}


function validate(val){
var checkVal_set = false;

if(val == '4'){
checkVal_set = validate_two();  
}else if(val != '4'){
checkVal_set = validate_one();
}else{
checkVal_set = false;
}


return checkVal_set;
}






function validate_two() {
var return_set = false;
jQuery('#err-msg').hide();
var sectorid = jQuery('#sector_id').val();
var propertyid = jQuery('#property_id').val();
var bookingstartdate = jQuery('#booking_start_date').val();
var starttime = jQuery('#start_time').val();
var endtime = jQuery('#end_time').val();








if(
sectorid == '' ||  
propertyid == '' || 
bookingstartdate == '' || 
starttime == '' ||  
endtime == '' 
) {

jQuery('#err-msg').show().html('All fields required !');
alert('All fields required !');


}else if(ConvertTimeformat("24", starttime) > ConvertTimeformat("24", endtime)){
jQuery('#err-msg').show().html('Start and End times are wrong !');
alert('Start and End times are wrong !');
}else if((ConvertTimeformat("24", endtime) - ConvertTimeformat("24", starttime)) < 4){
jQuery('#err-msg').show().html('Please select more than four hours to allocate the Auditorium !');
alert('Please select more than four hours to allocate the Auditorium !');
}else{


return_set = true;  
jQuery('#err-msg').hide();
}


return return_set;
}




function validate_one() {
var return_set = false;
jQuery('#err-msg').hide();

var sectorid = jQuery('#sector_id').val();
var propertyid = jQuery('#property_id').val();
var bookingstartdate = jQuery('#booking_start_date').val();
var bookingenddate = jQuery('#booking_end_date').val();
//var booking_no_of_dates = jQuery('#booking_no_of_dates').val();



if(
sectorid == '' ||  
propertyid == '' || 
bookingstartdate == '' || 
bookingenddate == ''

) {

jQuery('#err-msg').show().html('All fields required !');
alert('All fields required !');

}else if(!(bookingstartdate <= bookingenddate)){
jQuery('#err-msg').show().html('Start and End Dates are wrong !');
alert('Start and End Dates are wrong !');

}else{
return_set = true;	
jQuery('#err-msg').hide();
}



return return_set;
}





/* check available rooms */
//$scope.avail_room = jQuery('#number_of_rooms').val();
$scope.check_available_room = function(EnterRoom,pid,availableRooms,total){

//console.log(EnterRoom);
//console.log(pid);
//console.log(parseInt(availableRooms));

var inputName ='number_of_room_'+pid;
//var roomVal = jQuery('#'+inputName).val();

if(parseInt(EnterRoom) > parseInt(availableRooms)   ){
alert('Incorrect value. Please try again !');
jQuery('#'+inputName).val('');
jQuery('#roomval_'+pid).text(total);
//$scope.fields.number_of_room_xx = "";	
}else{


var balRoom = parseInt(availableRooms) - parseInt(EnterRoom);
jQuery('#roomval_'+pid).text(balRoom);



}


/*$scope.avail_room = jQuery('#number_of_room_'+pid).val();


var room_avai = "";
var default_val = parseInt(jQuery('#number_of_room_'+pid).val()); // convert to int



if (room <= default_val) { 
room_avai = default_val - room;
}else{
alert('Incorrect value. Please try again !');
//$scope.fields.number_of_room_xx = "";	
room_avai = default_val;
}*/



/*acc = 'varname';
str = acc+' = '+'123';
eval(str)
alert(varname);*/


/*console.log(default_val);
console.log(room_avai);
console.log(room);*/


//$scope.avail_room = room_avai;
//console.log(room);
}
/*###############################*/










// date
$scope.fieldValues = {
dateOfBirth: ""
};

/*Date*/

$scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
$scope.months = [{id: 1, name:"January"},
{id: 2, name:"February"},
{id: 3, name:"March"},
{id: 4, name:"April"},
{id: 5, name:"May"},
{id: 6, name:"June"},
{id: 7, name:"July"},
{id: 8, name:"August"},
{id: 9, name:"September"},
{id: 10, name:"October"},
{id: 11, name:"November"},
{id: 12, name:"December"}
];


$scope.years = [];
var d = new Date();
for (var i = (d.getFullYear() + 1); i > (d.getFullYear() - 1); i--) {
$scope.years.push(i);
}




$scope.year = "";
$scope.month = "";
$scope.day = "";

$scope.updateDate = function (input) {	
if (input == "year"){
$scope.month = "";
$scope.day = "";
}
else if (input == "month"){
$scope.day = "";
}
if ($scope.year && $scope.month && $scope.day){
$scope.fieldValues.dateOfBirth = new Date($scope.year, $scope.month.id - 1, $scope.day);
}

};


// date


















$scope.action = {};

$scope.checkCaptcha = function() {

/*if($captcha.checkResult($scope.resultado)){
$scope.captchaerror = false;
}else{
$scope.captchaerror = true;	
}*/

};


getSector();


$scope.options2 = []; // we'll get these later
$scope.getOptions2 = function(sid,pid){


getProperty(sid,pid);


};








function getProperty(sid,pid) {

$scope.method = 'GET';
$scope.url = 'index.php?option=com_ibooking&task=booking_registration.getProperty&sid='+sid+'&pid='+pid;
$http({method: $scope.method, url: $scope.url}). //, cache: $templateCache
then(function(response) {
$scope.status = response.status;

//$scope.fields = response.data.data;

$scope.items2 = response.data;

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});
}





getPurpose();
function getPurpose() {
var property_type_id = jQuery('#property_type_id').val();

$scope.method = 'GET';
$scope.url = 'index.php?option=com_ibooking&task=booking_registration.getPurpose&pid='+property_type_id;
$http({method: $scope.method, url: $scope.url}). //, cache: $templateCache
then(function(response) {
$scope.status = response.status;

//$scope.fields = response.data.data;
$scope.items3 = response.data;

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});
}










function getSector() {

$scope.method = 'GET';
$scope.url = 'index.php?option=com_ibooking&task=booking_registration.getSector';
$http({method: $scope.method, url: $scope.url}). //, cache: $templateCache
then(function(response) {
$scope.status = response.status;

$scope.items =response.data;

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});
}















$scope.submit_form = function() {

//correct
if($captcha.checkResult($scope.resultado) == true)
{
////////////#################
$scope.method = 'POST';
$scope.url = 'index.php?option=com_neda&task=entrepreneur_registration.ibookingRegistration';
$scope.dataArray = {data:$scope.fields};

$http({method: $scope.method, url: $scope.url,data: $scope.dataArray}). 
then(function(response) {
$scope.status = response.status;
if(response.data.state==false){
// check existing data
alert(response.data.msg);
//jQuery('.err_msg').show().html(response.data.msg);

}else if(response.data.state==true){
// success
alert('Successfully registered.');
$window.location.href = 'index.php';
}else{
// error
alert('Registration Error occured !');
$window.location.href = 'index.php';
}

}, function(response) {
$scope.data = response.data || "Request failed";
$scope.status = response.status;
});
////////////#################
}
//error captcha
else
{
alert("Captcha Error: Please enter correct captcha !");
}


};
///////////////////##############-end-################//////////////////////////////////////






}); // app controller




app.filter('validMonths', function () {
return function (months, year) {
var filtered = [];
var now = new Date();
var over18Month = now.getUTCMonth() + 1;
var over18Year = now.getUTCFullYear() - 18;
if(year != ""){
if(year == over18Year){
angular.forEach(months, function (month) {
if (month.id <= over18Month) {
filtered.push(month);
}
});
}
else{
angular.forEach(months, function (month) {
filtered.push(month);
});
}
}
return filtered;
};
});

app.filter('daysInMonth', function () {
return function (days, year, month) {
var filtered = [];
angular.forEach(days, function (day) {
if (month != ""){
if (month.id == 1 || month.id == 3 || month.id == 5 || month.id == 7 || month.id == 8 || month.id == 10 || month.id == 12) {
filtered.push(day);
}
else if ((month.id == 4 || month.id == 6 || month.id == 9 || month.id == 11) && day <= 30){
filtered.push(day);
}
else if (month.id == 2){
if (year % 4 == 0 && day <= 29){
filtered.push(day);
}
else if (day <= 28){
filtered.push(day);
}
}
}
});
return filtered;
};
});

app.filter('validDays', function () {
return function (days, year, month) {
var filtered = [];
var now = new Date();
var over18Day = now.getUTCDate();
var over18Month = now.getUTCMonth() + 1;
var over18Year = now.getUTCFullYear() - 18;
if(year == over18Year && month.id == over18Month){
angular.forEach(days, function (day) {
if (day <= over18Day) {
filtered.push(day);
}
});
}
else{
angular.forEach(days, function (day) {
filtered.push(day);
});
}
return filtered;
};
});

function changeMe(sel)
{
sel.style.color = "#000";            
}






function Base64(val,state){

var Base64 = {


_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",


encode: function(input) {
var output = "";
var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
var i = 0;

input = Base64._utf8_encode(input);

while (i < input.length) {

chr1 = input.charCodeAt(i++);
chr2 = input.charCodeAt(i++);
chr3 = input.charCodeAt(i++);

enc1 = chr1 >> 2;
enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
enc4 = chr3 & 63;

if (isNaN(chr2)) {
enc3 = enc4 = 64;
} else if (isNaN(chr3)) {
enc4 = 64;
}

output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

}

return output;
},


decode: function(input) {
var output = "";
var chr1, chr2, chr3;
var enc1, enc2, enc3, enc4;
var i = 0;

input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

while (i < input.length) {

enc1 = this._keyStr.indexOf(input.charAt(i++));
enc2 = this._keyStr.indexOf(input.charAt(i++));
enc3 = this._keyStr.indexOf(input.charAt(i++));
enc4 = this._keyStr.indexOf(input.charAt(i++));

chr1 = (enc1 << 2) | (enc2 >> 4);
chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
chr3 = ((enc3 & 3) << 6) | enc4;

output = output + String.fromCharCode(chr1);

if (enc3 != 64) {
output = output + String.fromCharCode(chr2);
}
if (enc4 != 64) {
output = output + String.fromCharCode(chr3);
}

}

output = Base64._utf8_decode(output);

return output;

},

_utf8_encode: function(string) {
string = string.replace(/\r\n/g, "\n");
var utftext = "";

for (var n = 0; n < string.length; n++) {

var c = string.charCodeAt(n);

if (c < 128) {
utftext += String.fromCharCode(c);
}
else if ((c > 127) && (c < 2048)) {
utftext += String.fromCharCode((c >> 6) | 192);
utftext += String.fromCharCode((c & 63) | 128);
}
else {
utftext += String.fromCharCode((c >> 12) | 224);
utftext += String.fromCharCode(((c >> 6) & 63) | 128);
utftext += String.fromCharCode((c & 63) | 128);
}

}

return utftext;
},

_utf8_decode: function(utftext) {
var string = "";
var i = 0;
var c = c1 = c2 = 0;

while (i < utftext.length) {

c = utftext.charCodeAt(i);

if (c < 128) {
string += String.fromCharCode(c);
i++;
}
else if ((c > 191) && (c < 224)) {
c2 = utftext.charCodeAt(i + 1);
string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
i += 2;
}
else {
c2 = utftext.charCodeAt(i + 1);
c3 = utftext.charCodeAt(i + 2);
string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
i += 3;
}

}

return string;
}

}

if(state='encode'){
return Base64.encode(val);
}
if(state='decode'){
return Base64.decode(val);
}

}




})(window.angular);




